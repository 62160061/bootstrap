module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160061/learn_bootstrap/'
      : '/'
}
